package cn.javacoder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.support.spring.messaging.MappingFastJsonMessageConverter;
import org.springframework.http.HttpHeaders;
import org.springframework.messaging.converter.StringMessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;


/**
 * Hello world!
 *
 */
public class Client
{
    public static void main( String[] args ) throws Exception {

        User user = new User();
        user.setUsername("Rex");
        user.setPassword("Rex");

        String response = MiscUtils.post("http://localhost:8080/login", JSON.toJSONString(user));
        JSONObject object = JSON.parseObject(response);
        if(object.getInteger("code") != 200) {
            throw new RuntimeException("登录失败");
        }
        String token =  object.getString("data");


        WebSocketClient webSocketClient = new StandardWebSocketClient();
        WebSocketStompClient stompClient = new WebSocketStompClient(webSocketClient);
        stompClient.setMessageConverter(new StringMessageConverter());
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.initialize();
        //stompClient.setTaskScheduler(scheduler); // for heartbeats
        //String url = "ws://localhost:8080/robot-work/endpoint?Authorization="+ token;
        String url = "ws://localhost:8080/endpoint";
        WebSocketHttpHeaders wsHeaders = new WebSocketHttpHeaders();
        wsHeaders.add("token", token);
        stompClient.connect(url, wsHeaders, new MyStompSessionHandler());
        System.in.read();
    }
}
