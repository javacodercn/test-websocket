package cn.javacoder;

import lombok.Data;

@Data
public class BasicRobot {
    private String robotSn;
    private String robotType;
    private String robotName;
    private Integer voiceVol;
    private String status;

}
