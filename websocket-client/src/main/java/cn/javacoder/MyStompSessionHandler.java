package cn.javacoder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.*;

import java.lang.reflect.Type;

@Slf4j
public class MyStompSessionHandler extends StompSessionHandlerAdapter {

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        // 订阅管理员对机器人名称， 音量的更改
        session.subscribe("/user/queue/RobotAction", new ActionFrameHandler(session));
        //订阅人工客服对业务咨询的回复
        session.subscribe("/user/queue/NewResponse", new ConsultMessageFrameHandler());
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        log.error(session.getSessionId() + ", messageType:" + command.getMessageType() , exception);
        super.handleException(session, command, headers, payload, exception);
    }
}
