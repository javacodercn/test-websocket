package cn.javacoder.engine;

import lombok.Data;

@Data
public class Result {
    /*文字*/
    private String text;
    /*图片*/
    private String pic;
    /*表情*/
    private String emoji;
    /*语音播报*/
    private String voice;
}
