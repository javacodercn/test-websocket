package cn.javacoder.engine;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Action {
    /*CLOSE_SESSION*/
    private String type;
    private String value;
    private String success;
    private String failure;
    private String id;
    private String from;

    public static class ActionList extends ArrayList<Action> {

    }
}
