//https://stomp-js.github.io/guide/stompjs/using-stompjs-v5.html
var user = {
    "username":"ebotStaff",
    "password":"123456"
}
user = JSON.stringify(user);

var xhr = new XMLHttpRequest();
xhr.open("POST", '/auth/login', true);
xhr.setRequestHeader('Content-Type', 'application/json');
xhr.responseType = 'json';
xhr.onreadystatechange = function (ev) {
    var token = xhr.response.data['access_token'];
    triggerWebsocket(token)
}
xhr.send(user);

function triggerWebsocket(token) {
    var socket = new WebSocket("ws://localhost:8080/robot-work/endpoint?Authorization=" + token);
    var stompClient = webstomp.over(socket);

    stompClient.connect({}, function(frame) {
        stompClient.subscribe("/topic/NewRequest", function(frame){
            console.log(frame);
        }, {});

        stompClient.subscribe("/user/queue/NewMessages", function(frame){
            console.log(frame);
        }, {});
    });
}
