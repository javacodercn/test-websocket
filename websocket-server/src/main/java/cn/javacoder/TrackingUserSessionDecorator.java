package cn.javacoder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.WebSocketHandlerDecorator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 跟踪用户webSocket session
 */
@Slf4j
public class TrackingUserSessionDecorator extends WebSocketHandlerDecorator {

    /**
     * 统计有活动session 的用户
     */
    private Map<String, AtomicInteger> userSessionCountMap = new HashMap<String, AtomicInteger>();

    /**
     * 触发某个业务的监控
     */
    private Set<String> kickoffMonitors   = new ConcurrentSkipListSet<>();

    public boolean needPush(String username, String queue, boolean considerBusiness) {
        if(considerBusiness) {
            String key = username + queue;
            return userSessionCountMap.getOrDefault(username, new AtomicInteger(0)).get() > 0
                    && kickoffMonitors.contains(key);
        }
        return userSessionCountMap.getOrDefault(username, new AtomicInteger(0)).get() > 0;
    }

    public void kickoffMonitor(String username, String queue) {
        String key = username + queue;
        kickoffMonitors.add(key);
    }


    public TrackingUserSessionDecorator(WebSocketHandler delegate) {
        super(delegate);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        if(session.getPrincipal() != null) {
            String username = session.getPrincipal().getName();
            userSessionCountMap.putIfAbsent(username, new AtomicInteger(0));
            userSessionCountMap.get(username).incrementAndGet();
        }
        super.afterConnectionEstablished(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        if(session.getPrincipal() != null) {
            String username = session.getPrincipal().getName();
            int refCount = userSessionCountMap.getOrDefault(username, new AtomicInteger(0)).decrementAndGet();
            if (refCount <= 0) {
                log.info("remove user:{} from kickoffMonitors", username);
                kickoffMonitors.remove(username);
            }
        }
        super.afterConnectionClosed(session, closeStatus);
    }
}
