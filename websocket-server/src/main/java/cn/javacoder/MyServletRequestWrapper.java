package cn.javacoder;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.security.Principal;

@Slf4j
public class MyServletRequestWrapper extends HttpServletRequestWrapper {

    public MyServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public Principal getUserPrincipal() {
        HttpServletRequest request = (HttpServletRequest)getRequest();
        String token = request.getHeader("token");
        if(token != null && !token.isEmpty()) {
            Principal principal = new User(token);
            log.info(principal.getName());
            return principal;
        }
        return null;
    }
}
