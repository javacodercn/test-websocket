package cn.javacoder;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class R<T> {
    public static final int OK = 200;
    public static final int FAILURE = 500;
    private int code ;
    private String msg;
    private T data;

    public static R fail(String msg) {
        return R.builder().code(FAILURE).msg(msg).build();
    }

    public static <T> R ok(T data) {
        return R.builder().code(OK).data(data).build();
    }

}
