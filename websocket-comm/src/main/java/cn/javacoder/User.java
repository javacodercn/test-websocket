package cn.javacoder;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.security.Principal;
import java.util.Objects;

@Data
@NoArgsConstructor
public class User implements Principal {

    private String username;

    private String password;

    public User(String token) {
        if(token != null && !token.isEmpty()) {
            String[] arrs = token.split("\\|");
            this.username = arrs[0];
        }
    }

    @Override
    public String getName() {
        return username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
